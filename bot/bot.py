import telebot
from django.conf import settings
from django.http import HttpResponse
from .models import *
from telebot import types

bot = telebot.TeleBot(settings.TELEGRAM_TOKEN)


def webhook(request):
    print('Webhook received request')
    if request.method == 'POST':
        json_string = request.body.decode("utf-8")
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])

        return HttpResponse(status=200)
    return HttpResponse("BOT IS LIVE HERE")


@bot.message_handler(commands=['start'])
def start(message):
    get_user(message)
    markup_inline = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)

    items = ['Python', 'PHP', 'C#', 'JavaScript']
    markup_inline.add(*[types.KeyboardButton(item) for item in items])

    bot.send_message(message.chat.id, 'Are you a developer?', reply_markup=markup_inline)


@bot.message_handler(content_types=['text'])
def answer(message):
    print('answer')
    markup = types.ReplyKeyboardRemove(selective=False)
    bot.send_message(message.chat.id, 'Full Name:', reply_markup=markup)
    bot.register_next_step_handler(message, get_info)


@bot.message_handler(content_types=['text'])
def get_info(message):
    print('get_info')
    user = get_user(message)
    user.full_name = message.text
    user.phone_number = message.text
    user.save()

    bot.send_message(message.chat.id, 'Phone number:')
    markup_inline = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    contact_number = types.KeyboardButton('Share my number', request_contact=True)
    markup_inline.add(contact_number)
    bot.send_message(message.chat.id, 'Type your number or share your contact', reply_markup=markup_inline)
    bot.register_next_step_handler(message, user_resume)


@bot.message_handler(content_types=['text'])
def user_resume(message):
    print('user_resume')
    user = get_user(message)
    print('1st step')

    if message.text is None:
        phone = message.contact.phone_number
        user.phone_number = phone
        user.save()
    else:
        phone_number = message.text
        user.phone_number = phone_number
        user.save()

    markup = types.ReplyKeyboardRemove(selective=False)
    print('2nd step')
    bot.send_message(message.chat.id, 'Please send your resume!', reply_markup=markup)
    bot.register_next_step_handler(message, info_end)


@bot.message_handler(content_types=['document'])
def info_end(message):
    print('info_end')

    print(message.content_type)
    if message.content_type == 'document':
        user = get_user(message)

        user.resume = message.document.file_id
        user.save()
        bot.send_message(message.chat.id, 'Thank you, soon we will contact you')
    else:
        user_resume(message)

    # markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    # markup.add(types.KeyboardButton(text='Back main menu'))
    # bot.send_message(message.chat.id, 'If you want to back main menu, click the button below!', reply_markup=markup)

    # markup = types.InlineKeyboardMarkup()
    # markup.add(types.InlineKeyboardButton(text='Back main menu', callback_data='home'))
    # bot.send_message(message.chat.id, 'If you want to back main menu, click the button below!', reply_markup=markup)


def get_user(message):
    user = TelegramUser.objects.filter(user_id=message.chat.id).first()

    if not user:
        user = TelegramUser.objects.create(user_id=message.chat.id)

    return user
